SLIRPHELPER:=target/debug/libslirp-helper

$(SLIRPHELPER):
	cargo build --features=all

.PHONY: test
test: $(SLIRPHELPER)
	SLIRPHELPER=$(SLIRPHELPER) \
	PYTHONPATH=. \
	PYTHONIOENCODING=utf-8 \
		unshare -Ur python3 -m unittest
