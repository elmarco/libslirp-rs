extern crate cc;
extern crate pkg_config;

fn main() {
    let gio = pkg_config::Config::new().probe("gio-unix-2.0").unwrap();
    let slirp = pkg_config::Config::new().probe("slirp").unwrap();
    let mut build = cc::Build::new();

    for path in gio.include_paths.iter() {
        build.include(path);
    }
    for path in slirp.include_paths.iter() {
        build.include(path);
    }

    build
        .file("src/bin/dbus-p2p.c")
        .file("src/bin/dbus-vmstate1.c")
        .compile("dbus-p2p");
}
